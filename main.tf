terraform {

  required_providers {

    yandex = {

      source  = "yandex-cloud/yandex"
      version = "~> 0.75.0"
    }
  }

  required_version = ">= 0.14"
  experiments      = [ module_variable_optional_attrs ]
}


provider "yandex" {

  token = var.token
}


module "registry" {

  source          = "./modules/registry"

  registry_config = var.registry_config
}


module "network" {

  source = "./modules/network"
  
  network = var.network
}


module "objectstorage" {

  source                   = "./modules/objectstorage"
  buckets_service_accounts = var.buckets_service_accounts
  buckets                  = var.buckets
}


module "k8s" {

  source = "./modules/k8s"

  k8s_config = var.k8s_config

  depends_on = [
    module.network
  ]
}


module "psql" {

  source = "./modules/psql"
  
  psql_config = var.psql_config
  databases   = var.databases

  depends_on = [
    module.network
  ]
}


module "mongo" {

  source = "./modules/mongo"
  
  mongo_config = var.mongo_config

  depends_on = [
    module.network
  ]
}
