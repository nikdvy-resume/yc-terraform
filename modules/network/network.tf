locals {

  networks = distinct(flatten([

    for network_name, network_config in var.network: {

      network_name  = network_name
      description  = network_config.description
      cloud_name   = network_config.cloud_name
      folder_name  = network_config.folder_name
    }
  ]))

  subnets = distinct(flatten([

    for network_name, network_config in var.network: [

      for subnet_name, subnet_config in network_config.subnets: {

        network_name      = network_name
        subnet_name       = subnet_name
        cloud_name        = network_config.cloud_name
        folder_name       = network_config.folder_name
        v4_cidr_blocks    = subnet_config.v4_cidr_blocks
        availability_zone = subnet_config.availability_zone
        domain            = network_config.domain
        dns_servers       = network_config.dns_servers
        ntp_servers       = network_config.ntp_servers
      }
    ]
  ]))
}


resource "yandex_vpc_network" "networks" {
  for_each    = { for entry in local.networks: "${entry.cloud_name}.${entry.folder_name}.${entry.network_name}" => entry }
  description = each.value.description

  name      = each.value.network_name
  folder_id = data.yandex_resourcemanager_folder.folder["${each.value.cloud_name}.${each.value.folder_name}"].id
}


resource "yandex_vpc_subnet" "subnets" {
  for_each = { for entry in local.subnets: "${entry.cloud_name}.${entry.folder_name}.${entry.network_name}.${entry.subnet_name}" => entry }

  name           = each.value.subnet_name
  v4_cidr_blocks = each.value.v4_cidr_blocks
  zone           = each.value.availability_zone
  network_id     = yandex_vpc_network.networks["${each.value.cloud_name}.${each.value.folder_name}.${each.value.network_name}"].id
  folder_id      = data.yandex_resourcemanager_folder.folder["${each.value.cloud_name}.${each.value.folder_name}"].id

  dhcp_options {
    domain_name         = each.value.domain
    domain_name_servers = each.value.dns_servers
    ntp_servers         = each.value.ntp_servers
  }
}
