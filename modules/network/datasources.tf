locals {

  network_data = distinct(flatten([

    for network_name, network_config in var.network: {

      cloud_name   = network_config.cloud_name
      folder_name  = network_config.folder_name
      network_name  = network_name
    }
  ]))

  clouds = distinct(flatten([
    
    for network_name, network_config in var.network: network_config.cloud_name
  ]))

  folders = distinct(flatten([
    
    for network_name, network_config in var.network: {
      
      folder_name = network_config.folder_name
      cloud_name = network_config.cloud_name
    }
  ]))
}


data "yandex_resourcemanager_cloud" "cloud" {
  for_each = toset(local.clouds)

  name = each.value
}


data "yandex_resourcemanager_folder" "folder" {
  for_each = { for entry in local.folders : "${entry.cloud_name}.${entry.folder_name}" => entry }
  
  name     = each.value.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.cloud["${each.value.cloud_name}"].id
}
