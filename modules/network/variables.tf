variable "network" {
  description = "Конфигурация сети инфраструктуры"

  type = map(object({

    # Ключ - имя сети
    description = string

    cloud_name   = string
    folder_name  = string

    domain      = optional(string)
    dns_servers = optional(list(string))
    ntp_servers = optional(list(string))

    # Ключ - имя подсети
    subnets = map(object({

      v4_cidr_blocks    = list(string)
      availability_zone = string
    }))

    nat_subnet = optional(string)
    nat_ip = optional(string)

    labels = map(string)
  }))
}
