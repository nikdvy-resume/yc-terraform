output "networks" {
    description = "Вывод информации о сетях"

    value = yandex_vpc_network.networks
}

output "subnets" {
    description = "Вывод информации о подсетях"

    value = yandex_vpc_subnet.subnets
}