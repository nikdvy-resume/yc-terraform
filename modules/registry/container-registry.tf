resource "yandex_container_registry" "registry" {
  
  name      = var.registry_config.registry_name
  folder_id = data.yandex_resourcemanager_folder.target_folder.id

  labels = var.registry_config.labels
}
