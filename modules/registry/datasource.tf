data "yandex_resourcemanager_cloud" "target_cloud" {

  name = var.registry_config.cloud_name
}


data "yandex_resourcemanager_folder" "target_folder" {
  
  name     = var.registry_config.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target_cloud.id
}

