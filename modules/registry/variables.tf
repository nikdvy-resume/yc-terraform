variable "registry_config" {

  type = object({

    registry_name = string
    cloud_name    = string
    folder_name   = string
    service_accounts = map(object({
      sa_name = string
      roles = list(string)
    }))

    labels        = map(string)
  })
}

