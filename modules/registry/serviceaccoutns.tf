locals {

    sa_names = toset(distinct(flatten([
        for sa_binding_key, sa_binding_config in var.registry_config.service_accounts: [
            sa_binding_config.sa_name
        ]
    ])))

    sa_bindings = distinct(flatten([
        
        for sa_binding_key, sa_binding_config in var.registry_config.service_accounts: [
            
            for role in sa_binding_config.roles: {
                
                sa_name = sa_binding_config.sa_name
                sa_role = role
            }
        ]
    ]))
}


resource "yandex_iam_service_account" "registry" {
  description = "Сервисный аккаунт для Container Registry"
  for_each    = local.sa_names

  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}


resource "yandex_resourcemanager_folder_iam_member" "k8s_nodes" {
  for_each = { for entry in local.sa_bindings: "${entry.sa_name}.${entry.sa_role}" => entry }

  role      = each.value.sa_role
  member    = "serviceAccount:${yandex_iam_service_account.registry["${each.value.sa_name}"].id}"
  folder_id = data.yandex_resourcemanager_folder.target_folder.id

  depends_on = [
    yandex_iam_service_account.registry
  ]
}

