%{ for zone_name, zone_config in internal_zones_list }
${ zone_name } {
  forward . ${ join(" ", zone_config.dns_servers_list) }
}
%{ endfor }

. {
  forward . ${ join(" ", yc_internal_dns_list) }
  health
}
