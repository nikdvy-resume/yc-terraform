[Unit]
Description=caching CoreDNS resolver
Wants=crio.service
After=crio.service

[Service]
ExecStart=podman run --name coredns -v /opt/coredns.conf:/Corefile:z --net host ${coredns_container_image} -conf /Corefile
ExecStop=-podman rm -f coredns

Restart=always
RestartSec=5s

[Install]
WantedBy=multi-user.target