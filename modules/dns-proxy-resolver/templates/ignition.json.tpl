{
  "ignition": {
    "version": "3.3.0"
  },

  "storage": {
    "files": [
      {
        "path": "/etc/NetworkManager/NetworkManager.conf",
        "overwrite": true,
        "user": {
          "name": "root"
        },
        "contents": {
          "source": "data:;base64,${ base64encode(networkmanager_conf_content) }"
        },
        "mode": 420
      },
      {
        "path": "/etc/resolv.conf",
        "overwrite": true,
        "user": {
          "name": "root"
        },
        "contents": {
          "source": "data:;base64,${ base64encode(etc_resolv_conf_content) }"
        },
        "mode": 420
      },
      {
        "overwrite": true,
        "path": "/opt/coredns.conf",
        "user": {
          "name": "root"
        },
        "contents": {
          "source": "data:;base64,${ base64encode(coredns_config_content) }"
        },
        "mode": 420
      }
    ]
  },

  "passwd": {
    "users": [
      {
        "name": "${coreos_ssh_user}",
        "sshAuthorizedKeys": ${ jsonencode(coreos_ssh_authorized_keys) }
    }
    ]
  },

  "systemd": {
    "units": [
      {
        "name": "zincati.service",
        "enabled": false,
        "dropins": [{
          "name": "disable-zincati.conf",
          "contents": "[Unit]\nConditionPathExists=/enoent\n"
        }]
      },
      {
        "name": "systemd-resolved.service",
        "enabled": false,
        "dropins": [{
          "name": "disable-systemd-resolved.conf",
          "contents": "[Unit]\nConditionPathExists=/enoent\n"
        }]
      },
      {
        "name": "coredns.service",
        "enabled": true,
        "contents": ${ jsonencode(coredns_service_content) }
      }
    ]
  }
}
