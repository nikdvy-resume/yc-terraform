data "yandex_resourcemanager_cloud" "target" {

  name = var.dns_resolver_config.cloud_name
}


data "yandex_resourcemanager_folder" "target" {

  name     = var.dns_resolver_config.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target.id
}


data "yandex_vpc_network" "default" {

  name      = var.dns_resolver_config.network_name
  folder_id = data.yandex_resourcemanager_folder.target.id
}


data "yandex_vpc_subnet" "default" {
  for_each = toset(var.dns_resolver_config.subnets_name)

  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.target.id
}

data "yandex_compute_image" "default" {
  image_id = var.dns_resolver_config.image_id
}
