resource "yandex_compute_instance" "dns" {
  for_each = data.yandex_vpc_subnet.default

  folder_id   = data.yandex_resourcemanager_folder.target.id
  zone        = each.value.zone
  name        = "${var.dns_resolver_config.entities_name}-${each.value.zone}"
  hostname    = "${var.dns_resolver_config.entities_name}-${each.value.zone}"
  platform_id = var.dns_resolver_config.instance_config.platform_id

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.default.id
      size     = var.dns_resolver_config.instance_config.disk_size
    }
  }

  network_interface {
    subnet_id = each.value.subnet_id
    nat       = false

    security_group_ids = [
      yandex_vpc_security_group.default.id
    ]
  }

  resources {
    cores  = var.dns_resolver_config.instance_config.cores
    memory = var.dns_resolver_config.instance_config.memory
  }

  metadata = {
    serial-port-enable = 1

    user-data = templatefile("${path.module}/templates/ignition.json.tpl", {
      coreos_ssh_user            = var.dns_resolver_config.ssh_user
      coreos_ssh_authorized_keys = coalescelist(split("\n", file(var.dns_resolver_config.path_to_ssh_file)))

      networkmanager_conf_content = file("${path.module}/templates/NetworkManager.conf.tpl")

      etc_resolv_conf_content = templatefile("${path.module}/templates/resolv.conf.tpl", {
        yc_internal_dns_list = [
          cidrhost(each.value.v4_cidr_blocks.0, 2),
        ]
      })

      coredns_config_content = templatefile("${path.module}/templates/coredns.conf.tpl", {
        yc_internal_dns_list = [
          cidrhost(each.value.v4_cidr_blocks.0, 2),
        ]

        internal_zones_list = var.dns_resolver_config.internal_dns_zones_list
      })

      coredns_service_content = templatefile("${path.module}/templates/unit.service.tpl", {
        coredns_container_image = var.dns_resolver_config.container_image
      })
    })
  }

  allow_stopping_for_update = var.dns_resolver_config.allow_vm_stopping_for_update
}
