output "dns" {
  value = [for subnet_name, dns_host in yandex_compute_instance.dns : {
    subnet_name = subnet_name,
    dns_resolver_host = dns_host.network_interface.0.ip_address,
  }]
}