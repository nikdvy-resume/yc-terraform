resource "yandex_vpc_security_group" "default" {
  name = var.dns_resolver_config.entities_name

  folder_id  = data.yandex_resourcemanager_folder.target.id
  network_id = data.yandex_vpc_network.default.id

  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }

  ingress {
    protocol       = "UDP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 53
  }

  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 53
  }

  egress {
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}
