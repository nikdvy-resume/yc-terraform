variable "dns_resolver_config" {
  description = "Конфигурация DNS резолверов"

  type = object({
    cloud_name    = string
    folder_name   = string
    image_id      = string
    network_name  = string
    subnets_name  = list(string)
    entities_name = string

    instance_config = object({
      platform_id = string
      cores       = number
      memory      = number
      disk_size   = number
    })

    ssh_user         = string
    path_to_ssh_file = string
    container_image  = string

    internal_dns_zones_list = map(object({
      dns_servers_list = list(string)
    }))

    allow_vm_stopping_for_update = bool
  })
}
