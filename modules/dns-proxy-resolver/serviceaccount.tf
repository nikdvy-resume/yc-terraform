resource "yandex_iam_service_account" "default" {
  folder_id = data.yandex_resourcemanager_folder.target.id

  name        = "${var.dns_resolver_config.entities_name}-${data.yandex_resourcemanager_folder.target.name}"
  description = "service account to manage ${var.dns_resolver_config.entities_name} VMs"
}
