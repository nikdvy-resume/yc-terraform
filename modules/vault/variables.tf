variable "token" {
  description = "Введите ваш токен OAuth (https://cloud.yandex.com/en/docs/iam/concepts/authorization/oauth-token) or IAM token:"

  type      = string
  sensitive = true
  default   = ""
}

variable "cloud_name" {
  description = "Имя облака"
  type        = string
}

variable "folder_name" {
  description = "Имя каталога"
  type        = string
}

variable "vault" {
  type = object({
    chart            = string # "vault" or path to folder with chart
    chart_name       = string
    chart_namespace  = string
    chart_repository = optional(string) # https://helm.releases.hashicorp.com or ""
    chart_values     = optional(string)
    chart_version    = optional(string) # version for 0.19.0 with vault 1.9.2 for yandex cloud
    k8s_cluster_name = string
    kubernetes_host  = optional(string)
  })
}
