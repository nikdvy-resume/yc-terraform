# Vault
## Cloud
* После деплоя vault выполнить следующие шаги
* Инициализировать vault-0
```bash
$ kubectl exec -ti vault-0 -n infra -- vault operator init
```
* Сохранить Root token и Recovery keys в надежное место;
* Присоединить остальные реплики к vaut-0
```bash
$ kubectl exec -ti vault-1 -n infra -- vault operator raft join http://vault-0.vault-internal:8200
$ kubectl exec -ti vault-2 -n infra -- vault operator raft join http://vault-0.vault-internal:8200
```