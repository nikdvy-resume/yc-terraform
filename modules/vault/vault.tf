resource "yandex_iam_service_account" "this" {
  name        = "vault-kms-${var.folder_name}"
  description = "service account to use kms key for autounseal vault"
  folder_id   = data.yandex_resourcemanager_folder.selected.id
}

resource "yandex_resourcemanager_folder_iam_member" "this" {
  depends_on = [yandex_iam_service_account.this]
  role       = "kms.keys.encrypterDecrypter"
  member     = "serviceAccount:${yandex_iam_service_account.this.id}"
  folder_id  = data.yandex_resourcemanager_folder.selected.id
}

resource "yandex_iam_service_account_key" "this" {
  depends_on         = [yandex_resourcemanager_folder_iam_member.this]
  service_account_id = yandex_iam_service_account.this.id
  description        = "key for service account"
  key_algorithm      = "RSA_2048"
}

resource "yandex_kms_symmetric_key" "this" {
  name              = "vault-kms-${var.folder_name}"
  description       = "For autounseal vault"
  default_algorithm = "AES_256"
  folder_id         = data.yandex_resourcemanager_folder.selected.id
}

resource "kubernetes_secret" "this" {
  depends_on = [
    yandex_iam_service_account_key.this,
    yandex_kms_symmetric_key.this
  ]
  metadata {
    name      = "kms-for-vault"
    namespace = var.vault.chart_namespace
  }

  data = {
    "YANDEXCLOUD_SERVICE_ACCOUNT_KEY_FILE" = <<EOT
${jsonencode({
    "id" : "${yandex_iam_service_account_key.this.id}",
    "service_account_id" : "${yandex_iam_service_account_key.this.service_account_id}",
    "created_at" : "${yandex_iam_service_account_key.this.created_at}",
    "key_algorithm" : "${yandex_iam_service_account_key.this.key_algorithm}",
    "public_key" : "${yandex_iam_service_account_key.this.public_key}",
    "private_key" : "${yandex_iam_service_account_key.this.private_key}"
})}
EOT
"YANDEXCLOUD_KMS_KEY_ID" = yandex_kms_symmetric_key.this.id
}

type = "Opaque"
}

resource "helm_release" "this" {
  depends_on = [
    kubernetes_secret.this,
  ]
  chart      = local.chart
  name       = var.vault.chart_name
  namespace  = var.vault.chart_namespace
  repository = local.repository
  values     = ["${file(local.values)}"]
  version    = local.version
}
