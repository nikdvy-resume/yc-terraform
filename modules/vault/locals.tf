locals {
    values = fileexists(var.vault.chart_values) ? var.vault.chart_values : ""
    chart = fileexists("${var.vault.chart}/Chart.yaml") ? var.vault.chart : "vault"
    repository = var.vault.chart_repository == null ? "" : var.vault.chart_repository
    version = var.vault.chart_version == null ? "" : var.vault.chart_version
}