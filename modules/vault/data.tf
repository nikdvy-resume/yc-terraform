data "yandex_resourcemanager_cloud" "selected" {
  name = var.cloud_name
}

data "yandex_resourcemanager_folder" "selected" {
  name     = var.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.selected.id
}

data "yandex_kubernetes_cluster" "selected" {
  name      = var.vault.k8s_cluster_name
  folder_id = data.yandex_resourcemanager_folder.selected.id
}