variable "buckets_service_accounts" {

  type = map(object({
    
    name        = string
    description = string

    roles = list(string)
  }))
}

variable "buckets" {

  type = object({

    cloud_name  = string
    folder_name = string
  
    default_bucket_name_prefix = optional(string)

    config = map(object({

      name  = string
      owner = string

      grants = map(object({

        permissions = list(string)
      }))

      lifecycle_rules = optional(list(object({

        id      = string
        enabled = bool

        abort_incomplete_multipart_upload_days = optional(number)

        transition = optional(object({

          days          = number
          storage_class = string
        }))

        expiration = optional(object({

          days = number
        }))
      })))
    }))
  })
}
