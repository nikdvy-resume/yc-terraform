locals {

  bucket_config = distinct(flatten([

    for sa_key, sa_config in var.buckets_service_accounts: [

      for bucket_key, bucket_value in var.buckets.config: {
        
        sa_key          = sa_key
        sa_name         = sa_config.name
        bucket_key      = bucket_key
        name            = bucket_value.name
        owner           = bucket_value.owner
        grants          = bucket_value.grants
        lifecycle_rules = bucket_value.lifecycle_rules
      }
    ]
  ]))
}

resource "yandex_storage_bucket" "bucket" {
  for_each = { for entry in local.bucket_config: "${entry.bucket_key}" => entry if entry.sa_name == entry.owner}

  bucket = "${var.buckets.default_bucket_name_prefix != null ? "${var.buckets.default_bucket_name_prefix}-" : ""}${each.value.name}"

  access_key = yandex_iam_service_account_static_access_key.s3-bucket-sa[each.value.sa_key].access_key
  secret_key = yandex_iam_service_account_static_access_key.s3-bucket-sa[each.value.sa_key].secret_key

  dynamic "grant" {

    for_each = each.value.grants

    content {

      id   = yandex_iam_service_account.s3-bucket-sa[each.value.sa_key].id
      type = "CanonicalUser"

      permissions = grant.value.permissions
    }
  }

  dynamic "lifecycle_rule" {
    for_each = coalesce(each.value.lifecycle_rules, [])

    content {

      id      = lifecycle_rule.value.id
      enabled = lifecycle_rule.value.enabled

      abort_incomplete_multipart_upload_days = lifecycle_rule.value.abort_incomplete_multipart_upload_days

      dynamic "transition" {
        for_each = coalesce(lifecycle_rule.value.transition, {})

        content {

          days          = lifecycle_rule.value.transition.days
          storage_class = lifecycle_rule.value.transition.storage_class
        }
      }

      dynamic "expiration" {

        for_each = coalesce(lifecycle_rule.value.expiration, {})

        content {
          
          days = lifecycle_rule.value.expiration.days
        }
      }
    }
  }

  depends_on = [
    
    yandex_iam_service_account.s3-bucket-sa,
    yandex_resourcemanager_folder_iam_member.s3-bucket-sa,
    yandex_iam_service_account_static_access_key.s3-bucket-sa
  ]
}
