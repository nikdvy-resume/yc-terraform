data "yandex_resourcemanager_cloud" "target_cloud" {

  name = var.buckets.cloud_name
}


data "yandex_resourcemanager_folder" "target_folder" {

  name     = var.buckets.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target_cloud.id
}
