locals {

  s3_sa_roles = distinct(flatten([

    for sa_key, sa_spec in var.buckets_service_accounts: [

      for folder_role in sa_spec.roles: {

        sa_name = sa_spec.name
        sa_key  = sa_key
        sa_role = folder_role
      }
    ]
  ]))
}



resource "yandex_iam_service_account" "s3-bucket-sa" {
  for_each = var.buckets_service_accounts
  
  description = each.value.description
  
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
  name      = each.value.name
}


resource "yandex_resourcemanager_folder_iam_member" "s3-bucket-sa" {
  for_each = { for entry in local.s3_sa_roles: "${entry.sa_key}.${entry.sa_role}" => entry }

  folder_id = data.yandex_resourcemanager_folder.target_folder.id

  role   = each.value.sa_role
  member = "serviceAccount:${yandex_iam_service_account.s3-bucket-sa[each.value.sa_key].id}"

  depends_on = [
    yandex_iam_service_account.s3-bucket-sa
  ]
}


resource "yandex_iam_service_account_static_access_key" "s3-bucket-sa" {
  for_each = var.buckets_service_accounts
  
  description        = "static access key for object storage"
  service_account_id = yandex_iam_service_account.s3-bucket-sa[each.key].id
}