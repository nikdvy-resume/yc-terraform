terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.11.0"
    }
    yandex = {

      source  = "yandex-cloud/yandex"
      version = "~> 0.75.0"
    }
  }

  required_version = ">= 0.14"
  experiments      = [module_variable_optional_attrs]
}

provider "kubernetes" {
  host                   = coalesce(var.fluent_bit.kubernetes_host, data.yandex_kubernetes_cluster.selected.master[0].internal_v4_endpoint)
  cluster_ca_certificate = data.yandex_kubernetes_cluster.selected.master[0].cluster_ca_certificate
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "yc"
    args = [
      "k8s",
      "create-token",
      "--cloud-id", data.yandex_resourcemanager_cloud.selected.id,
      "--folder-id", data.yandex_resourcemanager_folder.selected.id,
    ]
  }
}

resource "kubernetes_namespace" "this" {
  count = var.fluent_bit.create_ns == true ? 1 : 0
  metadata {
    name = var.fluent_bit.namespace
  }
}
