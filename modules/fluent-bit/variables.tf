variable "token" {
  description = "Введите ваш токен OAuth (https://cloud.yandex.com/en/docs/iam/concepts/authorization/oauth-token) or IAM token:"

  type      = string
  sensitive = true
  default   = ""
}

variable "cloud_name" {
  description = "Имя облака"
  type        = string
}

variable "folder_name" {
  description = "Имя каталога"
  type        = string
}

variable "fluent_bit" {
  type = object({
    namespace          = string
    create_ns          = bool
    logging_group_name = string
    k8s_cluster_name   = string
    kubernetes_host    = optional(string)
  })
}

variable "logging_groups_id" {
  type = string
}
