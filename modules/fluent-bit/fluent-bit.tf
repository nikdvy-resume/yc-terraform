resource "yandex_iam_service_account" "this" {
  name        = "fluent-bit-yc-service"
  description = "service account to write logs"
  folder_id   = data.yandex_resourcemanager_folder.selected.id
}

resource "yandex_resourcemanager_folder_iam_member" "this" {
  depends_on = [yandex_iam_service_account.this]
  role      = "logging.writer"
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
  folder_id = data.yandex_resourcemanager_folder.selected.id
}

resource "yandex_iam_service_account_key" "this" {
  depends_on = [yandex_resourcemanager_folder_iam_member.this]
  service_account_id = yandex_iam_service_account.this.id
  description        = "key for service account"
  key_algorithm      = "RSA_2048"
}

resource "kubernetes_secret" "this" {
  depends_on = [yandex_iam_service_account_key.this]
  metadata {
    name = "fluent-bit-iam-key"
    namespace = var.fluent_bit.namespace
  }

  data = {
    "key.json" = <<EOT
${jsonencode({
   "id": "${yandex_iam_service_account_key.this.id}",
   "service_account_id": "${yandex_iam_service_account_key.this.service_account_id}",
   "created_at": "${yandex_iam_service_account_key.this.created_at}",
   "key_algorithm": "${yandex_iam_service_account_key.this.key_algorithm}",
   "public_key": "${yandex_iam_service_account_key.this.public_key}",
   "private_key": "${yandex_iam_service_account_key.this.private_key}"
})}
EOT
  }

  type = "Opaque"
}

resource "kubernetes_manifest" "auth" {
  depends_on = [yandex_iam_service_account_key.this]
  for_each   = local.yaml_set
  manifest  = yamldecode(each.value)
}

resource "kubernetes_manifest" "configmap" {
  depends_on = [kubernetes_manifest.auth]
  manifest = yamldecode(templatefile("${path.module}/files/configmap.yaml", {
    namespace = var.fluent_bit.namespace
    group_id = var.logging_groups_id
    cluster_id = data.yandex_kubernetes_cluster.selected.cluster_id
  }))
}

resource "kubernetes_manifest" "daemonset" {
  depends_on = [kubernetes_manifest.configmap,kubernetes_secret.this]
  manifest = yamldecode(templatefile("${path.module}/files/daemonset.yaml", {
    namespace = var.fluent_bit.namespace
    secret_name = kubernetes_secret.this.metadata[0].name
  }))
}