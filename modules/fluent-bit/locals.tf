locals {
  splitted_list = split("---", templatefile("${path.module}/files/auth.yaml", {
    namespace = var.fluent_bit.namespace
  }))
  yaml_set = toset(slice(local.splitted_list, 1, length(local.splitted_list)))
}
