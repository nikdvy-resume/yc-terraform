variable "organization_id" {
  description = "ID организации"
  type        = string
}

variable "organization_owner_iam_token" {
  description = "IAM-токен владельца организации"
  type        = string

  sensitive = true
}

variable "organization_clouds" {
  description = "Описание облаков и папок, которые создаются/управляются из terraform"

  # map key is a unique cloud key
  type = map(object({
    name       : string
    description: string

    labels: map(string)

    # map key is a user email
    cloud_users: map(object({
      # list elements are user roles
      cloud_user_roles: list(string)
    }))

    # map key is a unique folder key
    cloud_folders: map(object({
      name       : string
      description: string

      labels: map(string)

      # map key is a user email
      folder_users: map(object({
        # list elements are user roles
        folder_user_roles: list(string)
      }))
    }))
  }))
}

variable "yc_billing_accounts_api" {
  description = "API YCloud по управлению платежными аккаунтами"

  type    = string
  default = "https://billing.api.cloud.yandex.net/billing/v1/billingAccounts"
}

