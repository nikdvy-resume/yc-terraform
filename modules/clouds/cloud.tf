data "http" "yc_billing_accounts" {

  url = var.yc_billing_accounts_api

  # Optional request headers
  request_headers = {
    Accept        = "application/json"
    Authorization = "Bearer ${var.organization_owner_iam_token}"
  }
}


resource "yandex_resourcemanager_cloud" "clouds" {
  for_each = var.organization_clouds

  description = each.value.description
  name        = each.value.name

  labels = each.value.labels

  organization_id = var.organization_id

  depends_on = [
    data.http.yc_billing_accounts
  ]
}


resource "null_resource" "bind_clouds_to_billing_account" {
  for_each = yandex_resourcemanager_cloud.clouds

  provisioner "local-exec" {

    command = "curl -sSL -XPOST ${var.yc_billing_accounts_api}/${jsondecode(data.http.yc_billing_accounts.body).billingAccounts.0.id}/billableObjectBindings -H \"Authorization: Bearer ${var.organization_owner_iam_token}\" -H \"Content-Type: application/json\" -d '{ \"billableObject\": { \"id\": \"${each.value.id}\", \"type\": \"cloud\" } }'"
  }

  depends_on = [
    yandex_resourcemanager_cloud.clouds
  ]
}


resource "time_sleep" "wait" {
  create_duration = "15s"

  depends_on = [
    yandex_resourcemanager_cloud.clouds,
    null_resource.bind_clouds_to_billing_account
  ]
}


output "clouds" {
  value = yandex_resourcemanager_cloud.clouds
}
