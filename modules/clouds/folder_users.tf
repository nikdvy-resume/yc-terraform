locals {

  cloud_folders_user_roles = distinct(flatten([
    for cloud_key, cloud_config in var.organization_clouds : [
      for folder_key, folder_spec in cloud_config.cloud_folders : [
        for user_email, user_spec in folder_spec.folder_users: [
          for folder_user_role in user_spec.folder_user_roles: {
            cloud_key        : cloud_key
            folder_key       : folder_key
            cloud_name       : cloud_config.name
            folder_name      : folder_spec.name
            folder_user_email: user_email
            folder_user_role : folder_user_role
          }
        ]
      ]
    ]
  ]))
}

resource "yandex_resourcemanager_folder_iam_member" "folder_user_roles" {
  for_each = { for entry in local.cloud_folders_user_roles: "${entry.cloud_key}.${entry.folder_key}.${entry.folder_user_email}.${entry.folder_user_role}" => entry }

  folder_id = yandex_resourcemanager_folder.cloud_folders["${each.value.cloud_key}.${each.value.folder_key}"].id

  member = "userAccount:${data.yandex_iam_user.cloud_users[each.value.folder_user_email].id}"
  role   = each.value.folder_user_role

  depends_on = [
    yandex_resourcemanager_folder.cloud_folders
  ]
}

