locals {

  cloud_users = distinct(flatten([

    for cloud_key, cloud_config in var.organization_clouds : [

      for user_email, user_roles in cloud_config.cloud_users : [

        {
          user_email: user_email
        }
      ]
    ]
  ]))

  cloud_folder_users = distinct(flatten([

    for cloud_key, cloud_config in var.organization_clouds : [

      for folder_key, folder_spec in cloud_config.cloud_folders : [

        for user_email, user_spec in folder_spec.folder_users: {
          
          user_email: user_email
        }
      ]
    ]
  ]))

  yandex_users = distinct(concat(local.cloud_users, local.cloud_folder_users))
}

data "yandex_iam_user" "cloud_users" {
  for_each = { for entry in local.yandex_users: entry.user_email => entry }

  login = each.value.user_email
}

output "cloud_yandex_users" {
  value = data.yandex_iam_user.cloud_users
}
