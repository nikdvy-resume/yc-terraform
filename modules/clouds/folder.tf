locals {

  cloud_folders = distinct(flatten([

    for cloud_key, cloud_config in var.organization_clouds : [

      for folder_key, folder_spec in cloud_config.cloud_folders : {

        cloud_key : cloud_key
        folder_key : folder_key
        cloud_name : cloud_config.name
        folder_name : folder_spec.name
        folder_description : folder_spec.description
        folder_labels : folder_spec.labels
      }
    ]
  ]))
}

resource "yandex_resourcemanager_folder" "cloud_folders" {
  for_each = { for entry in local.cloud_folders : "${entry.cloud_key}.${entry.folder_key}" => entry }

  description = each.value.folder_description

  cloud_id = yandex_resourcemanager_cloud.clouds[each.value.cloud_key].id
  name     = each.value.folder_name

  labels = each.value.folder_labels

  depends_on = [
    time_sleep.wait
  ]
}


output "cloud_folders" {
  value = yandex_resourcemanager_folder.cloud_folders
}
