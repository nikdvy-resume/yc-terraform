locals {

  cloud_users_roles = distinct(flatten([
    for cloud_key, cloud_config in var.organization_clouds : [
      for user_email, cloud_user_spec in cloud_config.cloud_users : [
        for cloud_user_role in cloud_user_spec.cloud_user_roles: {
          cloud_key       : cloud_key
          cloud_name      : cloud_config.name
          cloud_user_email: user_email
          cloud_user_role : cloud_user_role
        }
      ]
    ]
  ]))
}

resource "yandex_resourcemanager_cloud_iam_member" "cloud_user_roles" {
  for_each = { for entry in local.cloud_users_roles: "${entry.cloud_key}.${entry.cloud_user_email}.${entry.cloud_user_role}" => entry }

  cloud_id = yandex_resourcemanager_cloud.clouds[each.value.cloud_key].id
  role     = each.value.cloud_user_role

  member = "userAccount:${data.yandex_iam_user.cloud_users[each.value.cloud_user_email].id}"

  depends_on = [
    yandex_resourcemanager_cloud.clouds
  ]
}

output "cloud_users" {

  value = data.yandex_iam_user.cloud_users
}