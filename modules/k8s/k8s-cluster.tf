resource "yandex_kubernetes_cluster" "k8s_cluster" {

  for_each    = local.k8s_config_full
  description = each.value.cluster.description

  folder_id  = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  name       = each.value.cluster.cluster_name
  network_id = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id

  master {

    public_ip = false
    security_group_ids = [
      yandex_vpc_security_group.k8s-main-sg[each.key].id,
      yandex_vpc_security_group.k8s-master-whitelist[each.key].id
    ]
    regional {

      region = coalesce(each.value.cluster.region, var.defaults.cluster.region)

      dynamic "location" {

        for_each = { for entry in local.cluster_subnets : "${entry.cluster_key}.${entry.cloud_name}.${entry.folder_name}.${entry.network_name}.${entry.subnet_name}" => entry if entry.cluster_key == each.key }

        content {

          subnet_id = data.yandex_vpc_subnet.subnets[location.key].id
          zone      = data.yandex_vpc_subnet.subnets[location.key].zone
        }
      }
    }

    version = each.value.cluster.k8s_master_version

    maintenance_policy {

      auto_upgrade = coalesce(each.value.cluster.master_auto_upgrade, var.defaults.cluster.master_auto_upgrade)

      maintenance_window {

        day        = each.value.cluster.maintenance_window_day
        start_time = each.value.cluster.maintenance_window_start_time
        duration   = each.value.cluster.maintenance_window_duration
      }
    }
  }

  kms_provider {

    key_id = yandex_kms_symmetric_key.k8s_keys[each.value.cluster_key].id
  }

  cluster_ipv4_range       = each.value.cluster.cluster_ipv4_range
  node_ipv4_cidr_mask_size = coalesce(each.value.cluster.node_ipv4_cidr_mask_size, var.defaults.cluster.node_ipv4_cidr_mask_size)
  service_ipv4_range       = each.value.cluster.service_ipv4_range
  service_account_id       = yandex_iam_service_account.k8s_cluster[each.key].id
  node_service_account_id  = yandex_iam_service_account.k8s_nodes[each.key].id
  release_channel          = coalesce(each.value.cluster.release_channel, var.defaults.cluster.release_channel)
  network_policy_provider  = each.value.cluster.network_policy_provider == null ? "CALICO" : each.value.cluster.network_policy_provider == "CILIUM" ? null : each.value.cluster.network_policy_provider

  dynamic "network_implementation" {
    for_each = each.value.cluster.network_policy_provider == "CILIUM" ? ["1"] : []

    content {
      cilium {}
    }
  }

  labels = each.value.cluster.labels

  depends_on = [
    yandex_resourcemanager_folder_iam_member.k8s_cluster,
    yandex_resourcemanager_folder_iam_member.k8s_nodes,
    yandex_kms_symmetric_key.k8s_keys
  ]

  lifecycle {

    prevent_destroy = true
  }
}
