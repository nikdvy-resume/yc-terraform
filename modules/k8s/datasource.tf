data "yandex_resourcemanager_cloud" "target_cloud" {
  for_each = { for entry in local.allocation_data: "${entry.cluster_key}.${entry.cloud_name}" => entry }

  name = each.value.cloud_name
}


data "yandex_resourcemanager_folder" "target_folder" {
  for_each = { for entry in local.allocation_data: "${entry.cluster_key}.${entry.cloud_name}.${entry.folder_name}" => entry }

  name     = each.value.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target_cloud["${each.value.cluster_key}.${each.value.cloud_name}"].id
}


data "yandex_vpc_network" "networks" {
  for_each = { for entry in local.networks_data: "${entry.cluster_key}.${entry.cloud_name}.${entry.folder_name}.${entry.network_name}" => entry }

  name      = each.value.network_name
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.value.cluster_key}.${each.value.cloud_name}.${each.value.folder_name}"].id
}


data "yandex_vpc_subnet" "subnets" {
  for_each = { for entry in local.all_subnets : "${entry.cluster_key}.${entry.cloud_name}.${entry.folder_name}.${entry.network_name}.${entry.subnet_name}" => entry }

  name      = each.value.subnet_name
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.value.cluster_key}.${each.value.cloud_name}.${each.value.folder_name}"].id
}

data "local_file" "keys" {
  for_each    = local.k8s_config_full
  
  filename = "${path.root}/${each.value.ssh_keys_path}"
}

