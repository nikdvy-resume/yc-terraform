resource "yandex_vpc_security_group" "k8s-main-sg" {
  for_each    = local.k8s_config_full

  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените ее к кластеру и группам узлов."
  name        = "${each.value.cluster.cluster_name}-k8s-main-sg"
  folder_id   = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  network_id  = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id

  ingress {
    description    = "Правило разрешает проверки доступности с диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."

    protocol       = "TCP"
    v4_cidr_blocks = ["198.18.235.0/24", "198.18.248.0/24"]
    from_port      = 0
    to_port        = 65535
  }

  ingress {
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."

    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  ingress {
    description    = "Правило разрешает взаимодействие под-под и сервис-сервис. Укажите подсети вашего кластера и сервисов."

    protocol       = "ANY"

    v4_cidr_blocks = [

      each.value.cluster.cluster_ipv4_range,
      each.value.cluster.service_ipv4_range
    ]

    from_port      = 0
    to_port        = 65535
  }


  dynamic "ingress" {
    for_each = each.value.cluster.subnet_name

    content {
      description    = "Правило разрешает отладочные ICMP-пакеты."

      protocol       = "ICMP"

      v4_cidr_blocks = distinct(flatten([

        data.yandex_vpc_subnet.subnets["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}.${ingress.value}"].v4_cidr_blocks, 
        each.value.cluster.access_22_cidrs,
        each.value.cluster.access_API_cidrs
      ]))
    }
  }


  egress {
    description    = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Container Registry, Object Storage, Docker Hub и т. д."

    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "k8s-public-services" {
  for_each    = local.k8s_config_full

  description = "Правила группы разрешают подключение к сервисам из интернета. Примените правила только для групп узлов."
  name        = "${each.value.cluster.cluster_name}-k8s-public-services"
  folder_id   = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  network_id  = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id


  ingress {

    protocol       = "TCP"
    description    = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 30000
    to_port        = 32767
  }
}

resource "yandex_vpc_security_group" "k8s-nodes-ssh-access" {
  for_each    = local.k8s_config_full

  name        = "${each.value.cluster.cluster_name}-k8s-nodes-ssh-access"
  description = "Правила группы разрешают подключение к узлам кластера по SSH. Примените правила только для групп узлов."
  folder_id   = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  network_id  = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id


  ingress {

    protocol       = "TCP"
    description    = "Правило разрешает подключение к узлам по SSH с указанных IP-адресов."
    v4_cidr_blocks = each.value.cluster.access_22_cidrs
    port           = 22
  }
}

resource "yandex_vpc_security_group" "k8s-master-whitelist" {
  for_each    = local.k8s_config_full
  description = "Правила группы разрешают доступ к API Kubernetes из интернета. Примените правила только к кластеру."

  name        = "${each.value.cluster.cluster_name}-k8s-master-whitelist"
  folder_id   = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  network_id  = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id


  dynamic "ingress" {
    for_each = local.all_subnets

    content {

      protocol       = "TCP"
      description    = "Правило разрешает подключение к API Kubernetes через порт 6443."
      v4_cidr_blocks = distinct(flatten([
        data.yandex_vpc_subnet.subnets["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}.${ingress.value.subnet_name}"].v4_cidr_blocks,
        each.value.cluster.access_API_cidrs
      ]))
      port           = 6443
    }
  }


  dynamic "ingress" {
    for_each = each.value.cluster.subnet_name

    content {

      protocol       = "TCP"
      description    = "Правило разрешает подключение к API Kubernetes через порт 443 из указанной сети."
      v4_cidr_blocks = distinct(flatten([
        data.yandex_vpc_subnet.subnets["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}.${ingress.value}"].v4_cidr_blocks,
        each.value.cluster.access_API_cidrs
      ]))
      port           = 443
    }
  }
}

resource "yandex_vpc_security_group" "k8s-ingress-whitelist" {
  for_each    = local.k8s_config_full
  description = "Правила группы разрешают настраиваемый доступ к k8s извне"

  name        = "${each.value.cluster.cluster_name}-ingress-whitelist"
  folder_id   = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  network_id  = data.yandex_vpc_network.networks["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}.${each.value.cluster.network_name}"].id

  dynamic "ingress" {
    for_each = each.value.cluster.access_ingress_whitelist

    content {

      description = "Доступ по портам ${ingress.value.protocol} ${ingress.value.from_port}-${ingress.value.to_port} до k8s"

      protocol       = ingress.value.protocol
      v4_cidr_blocks = ingress.value.v4_cidr_blocks
      from_port      = ingress.value.from_port
      to_port        = ingress.value.to_port
    }
  }
}