resource "yandex_kubernetes_node_group" "node_groups" {

  for_each    = { for entry in local.k8s_nodes : "${entry.cluster_key}.${entry.node_group_key}.${entry.subnet_name}" => entry }
  description = each.value.description

  cluster_id  = yandex_kubernetes_cluster.k8s_cluster["${each.value.cluster_key}"].id
  name        = "${each.value.node_group_name}-${each.value.subnet_name}"
  version     = each.value.k8s_node_version
  node_labels = merge(each.value.node_roles, coalesce(each.value.node_labels, var.defaults.node_groups.node_labels))

  instance_template {

    platform_id = coalesce(each.value.instance_template_platform_id, var.defaults.node_groups.instance_template_platform_id)

    resources {

      memory = each.value.resources_memory
      cores  = each.value.resources_cpu_cores
    }

    boot_disk {

      type = each.value.boot_disk_type
      size = each.value.boot_disk_size
    }

    network_interface {

      subnet_ids = [

        data.yandex_vpc_subnet.subnets["${each.value.cluster_key}.${each.value.cloud_name}.${each.value.folder_name}.${each.value.network_name}.${each.value.subnet_name}"].id
      ]

      security_group_ids = [

        yandex_vpc_security_group.k8s-main-sg[each.value.cluster_key].id,
        yandex_vpc_security_group.k8s-nodes-ssh-access[each.value.cluster_key].id,
        yandex_vpc_security_group.k8s-public-services[each.value.cluster_key].id
      ]
    }

    scheduling_policy {

      preemptible = each.value.preemptible
    }

    metadata = {

      ssh-keys           = data.local_file.keys["${each.value.cluster_key}"].content
      serial-port-enable = 1
    }
  }

  scale_policy {

    auto_scale {

      initial = each.value.auto_scale_initial_count
      min     = each.value.auto_scale_min_count
      max     = each.value.auto_scale_max_count
    }
  }

  allocation_policy {

    location {

      zone = data.yandex_vpc_subnet.subnets["${each.value.cluster_key}.${each.value.cloud_name}.${each.value.folder_name}.${each.value.network_name}.${each.value.subnet_name}"].zone
    }
  }

  maintenance_policy {

    auto_upgrade = coalesce(each.value.maintenance_policy_auto_upgrade, var.defaults.node_groups.maintenance_policy_auto_upgrade)
    auto_repair  = coalesce(each.value.maintenance_policy_auto_repair, var.defaults.node_groups.maintenance_policy_auto_repair)

    maintenance_window {

      day        = each.value.maintenance_window_day
      start_time = each.value.maintenance_window_start_time
      duration   = each.value.maintenance_window_duration
    }
  }

  labels = each.value.labels

  depends_on = [
    yandex_kubernetes_cluster.k8s_cluster
  ]

  lifecycle {

    prevent_destroy = true
  }
}
