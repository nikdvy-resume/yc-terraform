resource "yandex_kms_symmetric_key" "k8s_keys" {
  for_each    = local.k8s_config_full
  description = "Ключ шифрования секретов k8s-кластера ${each.value.cluster.cluster_name}"
  
  folder_id         = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
  name              = "${each.value.cluster.cluster_name}-key"
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
}
