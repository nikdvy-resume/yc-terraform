variable "defaults" {
  description = "Список дефолтных переменных и их значений"

  type = object({

    cluster = object({

      region                   = string
      master_auto_upgrade      = bool
      node_ipv4_cidr_mask_size = number
      release_channel          = string
      network_policy_provider  = string
    })

    node_groups = object({

      instance_template_platform_id   = string
      maintenance_policy_auto_upgrade = bool
      maintenance_policy_auto_repair  = bool
      preemptible                     = bool
      node_labels                     = map(string)
    })
  })

  default = {

    cluster = {

      region                   = "ru-central1"
      master_auto_upgrade      = false
      node_ipv4_cidr_mask_size = 25
      release_channel          = "REGULAR"
      network_policy_provider =  "CALICO"
    }

    node_groups = {

      instance_template_platform_id   = "standard-v3"
      maintenance_policy_auto_upgrade = false
      maintenance_policy_auto_repair  = true
      preemptible                     = false
      node_labels                     = {}
    }
  }
}


variable "k8s_config" {
  description = "Переменная для конфигурирования кластера и его node groups"

  type = map(object({

    ssh_keys_path = string

    # Ключ - не имя кластера
    cluster = object({
      description = string

      cluster_name                  = string
      cluster_ipv4_range            = string
      service_ipv4_range            = string
      cloud_name                    = string
      folder_name                   = string
      network_name                  = string
      k8s_master_version            = string
      maintenance_window_day        = string
      maintenance_window_start_time = string
      maintenance_window_duration   = string
      subnet_name                   = list(string)
      access_22_cidrs               = list(string)
      access_API_cidrs              = list(string)

      access_ingress_whitelist = map(object({
        description    = string

        v4_cidr_blocks = list(string)
        from_port      = number
        to_port        = number
        protocol       = string
      }))

      labels = map(string)

      region                   = optional(string)
      master_auto_upgrade      = optional(bool)
      node_ipv4_cidr_mask_size = optional(number)
      release_channel          = optional(string)
      network_policy_provider  = optional(string)
    })

    # Ключ - не имя node group
    node_groups = map(object({
      description = string

      node_group_name               = string
      subnet_name                   = list(string)
      k8s_node_version              = string
      resources_memory              = number
      resources_cpu_cores           = number
      boot_disk_type                = string
      boot_disk_size                = number
      auto_scale_initial_count      = number
      auto_scale_min_count          = number
      auto_scale_max_count          = number
      maintenance_window_day        = string
      maintenance_window_start_time = string
      maintenance_window_duration   = string
      node_roles                    = list(string)

      labels = map(string)

      instance_template_platform_id   = optional(string)
      maintenance_policy_auto_upgrade = optional(bool)
      maintenance_policy_auto_repair  = optional(bool)
      preemptible                     = optional(bool)
      node_labels                     = optional(map(string))
    }))
  }))
}
