locals {

  allocation_data = distinct(flatten([

    for cluster_key, cluster_config in {

      for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          cluster_key = cluster_key
          cluster     = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }:

    {

      cluster_key  = cluster_key
      cluster_name = cluster_config.cluster.cluster_name
      cloud_name   = cluster_config.cluster.cloud_name
      folder_name  = cluster_config.cluster.folder_name
    }
  ]))


  networks_data = distinct(flatten([

    for cluster_key, cluster_config in var.k8s_config: [

      for node_group_key, node_group_config in cluster_config.node_groups: {

          cluster_key  = cluster_key
          cluster_name = cluster_config.cluster.cluster_name
          network_name = cluster_config.cluster.network_name
          cloud_name   = cluster_config.cluster.cloud_name
          folder_name  = cluster_config.cluster.folder_name
        }
      ]
    ]))


  subnets = distinct(flatten([

    for cluster_key, cluster_config in {

      for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          cluster_key = cluster_key
          cluster     = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }:

    [

      for subnet in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: [

          for node_group_key, node_group_config in cluster_config.node_groups:

            node_group_config.subnet_name

        ]
      ])):

      {

        cluster_key  = cluster_key
        cluster_name = cluster_config.cluster.cluster_name
        network_name = cluster_config.cluster.network_name
        subnet_name  = subnet
        cloud_name   = cluster_config.cluster.cloud_name
        folder_name  = cluster_config.cluster.folder_name
      }
    ]
  ]))

  cluster_subnets = distinct(flatten([

    for cluster_key, cluster_config in {

      for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          cluster_key = cluster_key
          cluster     = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }:

    [

      for subnet in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: [

            cluster_config.cluster.subnet_name
        ]
      ])):

      {

        cluster_key  = cluster_key
        cluster_name = cluster_config.cluster.cluster_name
        network_name = cluster_config.cluster.network_name
        subnet_name  = subnet
        cloud_name   = cluster_config.cluster.cloud_name
        folder_name  = cluster_config.cluster.folder_name
      }
    ]
  ]))



  k8s_nodes = distinct(flatten([

    for cluster_key, cluster_config in {

      for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          cluster_key = cluster_key
          cluster     = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }:

    [
      for node_group_key, node_group_config in cluster_config.node_groups: [

        for subnet_name in node_group_config.subnet_name: {
            description = node_group_config.description

            cluster_key                     = cluster_key
            cluster_name                    = cluster_config.cluster.cluster_name
            cloud_name                      = cluster_config.cluster.cloud_name
            folder_name                     = cluster_config.cluster.folder_name
            node_group_key                  = node_group_key
            node_group_name                 = node_group_config.node_group_name
            subnet_name                     = subnet_name
            network_name                    = cluster_config.cluster.network_name
            k8s_node_version                = node_group_config.k8s_node_version
            instance_template_platform_id   = node_group_config.instance_template_platform_id
            resources_memory                = node_group_config.resources_memory
            resources_cpu_cores             = node_group_config.resources_cpu_cores
            boot_disk_type                  = node_group_config.boot_disk_type
            boot_disk_size                  = node_group_config.boot_disk_size
            auto_scale_initial_count        = node_group_config.auto_scale_initial_count
            auto_scale_min_count            = node_group_config.auto_scale_min_count
            auto_scale_max_count            = node_group_config.auto_scale_max_count
            maintenance_policy_auto_upgrade = node_group_config.maintenance_policy_auto_upgrade
            maintenance_policy_auto_repair  = node_group_config.maintenance_policy_auto_repair
            maintenance_window_day          = node_group_config.maintenance_window_day
            maintenance_window_start_time   = node_group_config.maintenance_window_start_time
            maintenance_window_duration     = node_group_config.maintenance_window_duration
            preemptible                     = coalesce(node_group_config.preemptible, var.defaults.node_groups.preemptible)
            node_labels                     = coalesce(node_group_config.node_labels, var.defaults.node_groups.node_labels)

            labels = node_group_config.labels

            node_roles  = { for node_role in node_group_config.node_roles: "node.kubernetes.io/${node_role}" => "" }
            node_labels = coalesce(node_group_config.node_labels, var.defaults.node_groups.node_labels)
        }
      ]
    ]
  ]))


  k8s_config_full = {

    for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          ssh_keys_path = cluster_config.ssh_keys_path
          cluster_key   = cluster_key
          cluster       = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }


  all_subnets = distinct(flatten([

    for cluster_key, cluster_config in {

      for entry in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: {

          cluster_key = cluster_key
          cluster     = merge(var.defaults.cluster, cluster_config.cluster)

          node_groups = { for node_group_key, node_group_config in cluster_config.node_groups: node_group_key => merge(var.defaults.node_groups, node_group_config) }
        }
      ])) : entry.cluster_key => entry }:

    [

      for subnet in distinct(flatten([

        for cluster_key, cluster_config in var.k8s_config: [

          for node_group_key, node_group_config in cluster_config.node_groups: [

            cluster_config.cluster.subnet_name,
            node_group_config.subnet_name
          ]
        ]
      ])):

      {

        cluster_key  = cluster_key
        cluster_name = cluster_config.cluster.cluster_name
        network_name = cluster_config.cluster.network_name
        subnet_name  = subnet
        cloud_name   = cluster_config.cluster.cloud_name
        folder_name  = cluster_config.cluster.folder_name
      }
    ]
  ]))
}