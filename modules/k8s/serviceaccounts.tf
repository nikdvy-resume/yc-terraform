resource "yandex_iam_service_account" "k8s_cluster" {
  description = "Сервисный аккаунт k8s для управления ВМ"
  for_each    = var.k8s_config

  name      = "${each.value.cluster.cluster_name}-cluster"
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
}

resource "yandex_iam_service_account" "k8s_nodes" {
  description = "Сервисный аккаунт k8s-nodes (например, для пуша образов в Container Registry)"
  for_each    = var.k8s_config

  name      = "${each.value.cluster.cluster_name}-node-group"
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
}


resource "yandex_resourcemanager_folder_iam_member" "k8s_cluster" {
  for_each = var.k8s_config

  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_cluster["${each.key}"].id}"
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
}


resource "yandex_resourcemanager_folder_iam_member" "k8s_nodes" {
  for_each = var.k8s_config

  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.k8s_nodes["${each.key}"].id}"
  folder_id = data.yandex_resourcemanager_folder.target_folder["${each.key}.${each.value.cluster.cloud_name}.${each.value.cluster.folder_name}"].id
}

