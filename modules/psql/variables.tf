variable "defaults" {
  description = "Значения по-умолчанию для кластера"

  type = object({

    environment = string
    version     = string

    max_connections                   = number
    enable_parallel_hash              = bool
    vacuum_cleanup_index_scale_factor = number
    autovacuum_vacuum_scale_factor    = number
    default_transaction_isolation     = string
    shared_preload_libraries          = string
    
    pool_discard = bool
    pooling_mode = string

    lc_collate    = string
    lc_type       = string

    user_conn_limit = number
  })

  default = {

    environment = "PRODUCTION"
    version     = "14"

    max_connections                   = 395
    enable_parallel_hash              = true
    vacuum_cleanup_index_scale_factor = 0.2
    autovacuum_vacuum_scale_factor    = 0.34
    default_transaction_isolation     = "TRANSACTION_ISOLATION_READ_COMMITTED"
    shared_preload_libraries          = "SHARED_PRELOAD_LIBRARIES_AUTO_EXPLAIN,SHARED_PRELOAD_LIBRARIES_PG_HINT_PLAN"

    pool_discard = true
    pooling_mode = "SESSION"

    lc_collate = "en_US.UTF-8"
    lc_type    = "en_US.UTF-8"

    user_conn_limit = 3
  }
}


variable "psql_config" {
  description = "Переменная, описывающая конфигурацию кластеров постгреса"

  type = object({
    
    description = string

    cluster_name        = string
    cloud_name          = string
    folder_name         = string
    network_name        = string
    subnet_name         = list(string)
    access_6432_subnets = list(string)
    access_6432_cidrs   = optional(list(string))

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = object({

      type = string
      day  = string
      hour = number
    })
    
    users = map(object({

      username             = string
      password             = string
      conn_limit           = optional(number)
      accessible_databases = list(string)
      grants               = optional(list(string))
    }))

    pool_discard = optional(bool)
    pooling_mode = optional(string)

    environment = optional(string)
    version     = optional(number)
     
    max_connections                   = optional(number)
    enable_parallel_hash              = optional(bool)
    vacuum_cleanup_index_scale_factor = optional(number)
    autovacuum_vacuum_scale_factor    = optional(number)
    default_transaction_isolation     = optional(string)
    shared_preload_libraries          = optional(string)

    labels = optional(map(string))
  })
}


variable "databases" {
  description = "Конфигурация баз данных кластера psql"
  
  type = map(object({
    
    lc_collate    = optional(string)
    lc_type       = optional(string)
    database_name = string
    owner         = string
    extension     = list(string)
  }))
}
