data "yandex_resourcemanager_cloud" "target_cloud" {

  name = var.psql_config.cloud_name
}


data "yandex_resourcemanager_folder" "target_folder" {

  name     = var.psql_config.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target_cloud.id
}


data "yandex_vpc_network" "target_network" {
  
  name      = var.psql_config.network_name
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}


data "yandex_vpc_subnet" "target_subnet" {
  for_each = toset(var.psql_config.subnet_name)

  name      = each.key
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}


data "yandex_vpc_subnet" "access_6432_subnets" {
  for_each = toset(var.psql_config.access_6432_subnets)
  
  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}
