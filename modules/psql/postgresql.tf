locals {

  psql_config_full = merge(var.defaults, var.psql_config)
}

output "psql_config_full" {
  value = local.psql_config_full
}

resource "yandex_mdb_postgresql_cluster" "psql_cluster" {
  description = local.psql_config_full.description
  
  name        = local.psql_config_full.cluster_name
  network_id  = data.yandex_vpc_network.target_network.id
  folder_id   = data.yandex_resourcemanager_folder.target_folder.id
  environment = coalesce(local.psql_config_full.environment, var.defaults.environment)
  
  security_group_ids = [
    yandex_vpc_security_group.psql_sg.id
  ]

  config {
    version = coalesce(local.psql_config_full.version, var.defaults.version)

    resources {
      resource_preset_id = local.psql_config_full.resource_preset_id
      disk_type_id       = local.psql_config_full.disk_type_id
      disk_size          = local.psql_config_full.disk_size
    }

    postgresql_config = {
      max_connections                   = coalesce(local.psql_config_full.max_connections, var.defaults.max_connections)
      enable_parallel_hash              = coalesce(local.psql_config_full.enable_parallel_hash, var.defaults.enable_parallel_hash)
      vacuum_cleanup_index_scale_factor = coalesce(local.psql_config_full.vacuum_cleanup_index_scale_factor, var.defaults.vacuum_cleanup_index_scale_factor)
      autovacuum_vacuum_scale_factor    = coalesce(local.psql_config_full.autovacuum_vacuum_scale_factor, var.defaults.autovacuum_vacuum_scale_factor)
      default_transaction_isolation     = coalesce(local.psql_config_full.default_transaction_isolation, var.defaults.default_transaction_isolation)
      shared_preload_libraries          = coalesce(local.psql_config_full.shared_preload_libraries, var.defaults.shared_preload_libraries)
    }
  
    pooler_config {
      pool_discard = coalesce(var.psql_config.pool_discard, var.defaults.pool_discard)
      pooling_mode = coalesce(var.psql_config.pooling_mode, var.defaults.pooling_mode)
    }

    backup_window_start {
      hours   = local.psql_config_full.backup_window_start.hours
      minutes = local.psql_config_full.backup_window_start.minutes
    }
  }

    maintenance_window {
      type = local.psql_config_full.maintenance_window.type
      day  = local.psql_config_full.maintenance_window.day
      hour = local.psql_config_full.maintenance_window.hour
    }

  dynamic "database" {
    for_each = var.databases

    content {
      lc_collate = coalesce(database.value.lc_collate, var.defaults.lc_collate)
      lc_type    = coalesce(database.value.lc_type, var.defaults.lc_type)
      name       = database.value.database_name
      owner      = database.value.owner

      dynamic "extension" {
        for_each = database.value.extension == null ? ["pg_stat_statements"] : toset(database.value.extension)

        content {
          name = extension.key
        }
      }
    }
  }

  dynamic "user" {
    for_each = local.psql_config_full.users
    
    content {
      
      name       = user.value.username
      password   = user.value.password
      conn_limit = coalesce(user.value.conn_limit, var.defaults.user_conn_limit)

      dynamic "permission" {
        for_each = user.value.accessible_databases

        content {
          database_name = permission.value
        }
      }

      grants = user.value.grants
    }
  }

  dynamic "host" {
    for_each = local.psql_config_full.subnet_name

    content {
        zone             = data.yandex_vpc_subnet.target_subnet[host.value].zone
        subnet_id        = data.yandex_vpc_subnet.target_subnet[host.value].id
        assign_public_ip = false
    }
  }

  deletion_protection = true
}
