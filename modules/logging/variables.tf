variable "environment" {
  description = "Обычно имеет значения такие как test, dev, prod, etc."
  type        = string
}

variable "project" {
  description = "Имя проекта для которого применяется модуль."
  type        = string
}

variable "cloud_name" {
  description = "Имя облака"
  type        = string
}

variable "folder_name" {
  description = "Имя каталога"
  type        = string
}

variable "cloud_logging_config" {
  description = "Конфигуация для сервиса logging в Yandex Cloud"
  type = object({
    name             = string
    description      = optional(string)
    labels           = optional(map(string))
    retention_period = optional(string)
  })
}
