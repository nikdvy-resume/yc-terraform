terraform {
  required_providers {
    yandex = {

      source  = "yandex-cloud/yandex"
      version = "~> 0.75.0"
    }
  }

  required_version = ">= 0.14"
  experiments      = [module_variable_optional_attrs]
}

resource "yandex_logging_group" "this" {
  folder_id = data.yandex_resourcemanager_folder.selected.id

  name             = var.cloud_logging_config.name
  description      = coalesce(var.cloud_logging_config.description, local.default_description)
  retention_period = coalesce(var.cloud_logging_config.retention_period, local.default_retention_period)
  labels           = merge(local.common_labels, coalesce(var.cloud_logging_config.labels, local.default_labels))
}
