data "yandex_resourcemanager_cloud" "selected" {

  name = var.cloud_name
}


data "yandex_resourcemanager_folder" "selected" {

  name     = var.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.selected.id
}