locals {
  common_labels = {
    "terraform"   = "true",
    "environment" = var.environment,
    "project"     = var.project,
  }
  ############
  # Defaults
  ############
  default_retention_period = "72h"
  default_description      = "Empty description"
  default_labels           = {}
}
