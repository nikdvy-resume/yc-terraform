data "yandex_resourcemanager_cloud" "target_cloud" {

  name = var.mongo_config.cloud_name
}


data "yandex_resourcemanager_folder" "target_folder" {

  name     = var.mongo_config.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.target_cloud.id
}


data "yandex_vpc_network" "target_network" {
  
  name      = var.mongo_config.network_name
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}


data "yandex_vpc_subnet" "target_subnet" {
  for_each = toset(var.mongo_config.subnet_name)

  name      = each.key
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}


data "yandex_vpc_subnet" "access_27018_subnets" {
  for_each = toset(var.mongo_config.access_27018_subnets)
  
  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.target_folder.id
}
