variable "defaults" {
  description = "Значения по-умолчанию для кластера MongoDB"

  type = object({

    environment = string
    version     = string
  })

  default = {

    environment = "PRODUCTION"
    version     = "5.0"
  }
}

variable "mongo_config" {
  description = "Переменная, описывающая конфигурацию кластеров MongoDB"

  type = object({
    
    description = string

    cluster_name        = string
    cloud_name          = string
    folder_name         = string
    network_name        = string
    subnet_name         = list(string)
    access_27018_subnets = list(string)

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = object({

      type = string
      day  = string
      hour = number
    })
    
    databases = list(string)

    users = map(object({

      username             = string
      password             = string
      accessible_databases = set(object({
        database_name = string
        
        # https://cloud.yandex.com/en-ru/docs/managed-mongodb/concepts/users-and-roles
        roles         = list(string)
      }))
    })) 

    environment = optional(string)
    version     = optional(number)

    labels      = optional(map(string))
  })
}
