locals {

  mongo_config_full = merge(var.mongo_config, var.defaults)
}


resource "yandex_mdb_mongodb_cluster" "mongodb" {
  description = local.mongo_config_full.description

  name        = local.mongo_config_full.cluster_name
  environment = coalesce(local.mongo_config_full.environment, var.defaults.environment)
  network_id  = data.yandex_vpc_network.target_network.id
  folder_id  = data.yandex_resourcemanager_folder.target_folder.id

  cluster_config {
    version = coalesce(local.mongo_config_full.version, var.defaults.version)

    backup_window_start {

      hours   = local.mongo_config_full.backup_window_start.hours
      minutes = local.mongo_config_full.backup_window_start.minutes
    }
  }

  labels = local.mongo_config_full.labels


  dynamic "database" {

    for_each = local.mongo_config_full.databases

    content {
      
      name  = database.value
    }
  }
  
    dynamic "user" {

    for_each = local.mongo_config_full.users
    
    content {
      
      name     = user.value.username
      password = user.value.password

      dynamic "permission" {
        for_each = user.value.accessible_databases

        content {
          database_name = permission.value.database_name
          roles         = permission.value.roles
        }
      }
    }
  }

  resources {
    resource_preset_id = local.mongo_config_full.resource_preset_id
    disk_type_id       = local.mongo_config_full.disk_type_id
    disk_size          = local.mongo_config_full.disk_size
  }

  dynamic "host" {
    for_each = local.mongo_config_full.subnet_name
    
    content {

        zone_id          = data.yandex_vpc_subnet.target_subnet[host.value].zone
        subnet_id        = data.yandex_vpc_subnet.target_subnet[host.value].id
        assign_public_ip = false
    }
  }

  maintenance_window {
    
    type = local.mongo_config_full.maintenance_window.type
    day  = local.mongo_config_full.maintenance_window.day
    hour = local.mongo_config_full.maintenance_window.hour
  }

  security_group_ids = [
   
    yandex_vpc_security_group.mongodb_sg.id
  ]
  deletion_protection = true
}