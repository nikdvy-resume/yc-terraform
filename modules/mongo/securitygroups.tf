resource "yandex_vpc_security_group" "mongodb_sg" {
  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените ее к кластеру и группам узлов."
  
  name        = "${local.mongo_config_full.cluster_name}-main-sg"
  folder_id   = data.yandex_resourcemanager_folder.target_folder.id
  network_id  = data.yandex_vpc_network.target_network.id

  ingress {
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."

    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  dynamic "ingress" {
    for_each = var.mongo_config.access_27018_subnets

    content {
      description    = "Правило разрешает доступ по порту 27018."

      protocol       = "TCP"
      
      v4_cidr_blocks = distinct(flatten([

        data.yandex_vpc_subnet.access_27018_subnets[ingress.value].v4_cidr_blocks
      ]))
      
      port           = 27018
    }
  }

  egress {
    description    = "Правило разрешает весь исходящий трафик"

    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}