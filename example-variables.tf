# Каждое имя-ключ должно удовлетворять паттерну /[a-z]([-a-z0-9]{0,61}[a-z0-9])?/

variable "token" {
  description = "Введите ваш токен OAuth (https://cloud.yandex.com/en/docs/iam/concepts/authorization/oauth-token) or IAM token:"

  type      = string
  sensitive = true
}



##
## registry module variables
##

variable "registry_config" {

  type = object({

    registry_name = string
    cloud_name    = string
    folder_name   = string
    service_accounts = map(object({
      sa_name = string
      roles = list(string)
    }))

    labels = map(string)
  })

  default = {

    registry_name = "test-registry"
    cloud_name    = "mvp"
    folder_name   = "yc-dev"
    
    service_accounts = {
      
      sa-1 = {

        sa_name  = "puller-1"
        roles = [
          "container-registry.images.puller"
        ]
      }

      sa-2 = {

        sa_name  = "pusher-puller-1"
        roles = [
          "container-registry.images.puller",
          "container-registry.images.pusher"
        ]
      }
    }

    labels = {}
  }
}



##
## network module variables
##

# На данный момент не поддерживается изменение имени сети через скрипты
variable "network" {
  description = "Конфигурация сети инфраструктуры"
  
  type = map(object({
    
    # Ключ - имя сети
    description = string
    
    cloud_name   = string
    folder_name  = string

    # Ключ - имя подсети
    subnets = map(object({

      v4_cidr_blocks    = list(string)
      availability_zone = string
    }))
    
    nat_subnet = string
    nat_ip = string
    labels = map(string)
  }))

  default = {

    "k8s-network" = {
      description = "Сеть для k8s-кластера и его worker-nodes"

      cloud_name   = "mvp"
      folder_name  = "yc-dev"

      subnets = {

        "k8s-subnet-1" = {

          v4_cidr_blocks    = ["10.100.29.0/24"]
          availability_zone = "ru-central1-a"
        }

        "k8s-subnet-2" = {

          v4_cidr_blocks    = ["10.100.30.0/24"]
          availability_zone = "ru-central1-b"
        }

        "k8s-subnet-3" = {

          v4_cidr_blocks    = ["10.100.31.0/24"]
          availability_zone = "ru-central1-c"
        }
      }

      nat_subnet = "k8s-subnet-1"
      nat_ip = "10.100.29.3"
      labels = {}
    }

    "db-network" = {
      description = "Сеть для psql-кластера"
      
      cloud_name   = "mvp"
      folder_name  = "yc-dev"

      subnets = {
        
        "db-subnet-1" = {

          v4_cidr_blocks    = ["10.100.35.0/27"]
          availability_zone = "ru-central1-a"
        }

        "db-subnet-2" = {

          v4_cidr_blocks    = ["10.100.35.32/27"]
          availability_zone = "ru-central1-b"
        }

        "db-subnet-3" = {

          v4_cidr_blocks    = ["10.100.35.64/27"]
          availability_zone = "ru-central1-c"
        }
      }

      nat_subnet = "db-subnet-1"
      nat_ip = "10.100.35.3"
      labels = {}
    }

    "admin-network" = {
      description = "Сеть для админа"

      cloud_name   = "mvp"
      folder_name  = "yc-dev"

      subnets = {

        "admin-subnet" = {

          v4_cidr_blocks    = ["10.10.10.0/24"]
          availability_zone = "ru-central1-a"
        }
      }

      nat_subnet = "admin-subnet"
      nat_ip = "10.10.10.3"
      labels = {}
    }
  }
}


##
## k8s module variables
##

variable "defaults" {
  description = "Список дефолтных переменных и их значений"

  type = object({

    cluster = object({

      region                   = string
      master_auto_upgrade      = bool
      node_ipv4_cidr_mask_size = number
      release_channel          = string
      network_policy_provider  = string
    })

    node_groups = object({

      instance_template_platform_id   = string
      maintenance_policy_auto_upgrade = bool
      maintenance_policy_auto_repair  = bool
      preemptible                     = bool
      node_labels                     = map(string)
    })
  })

  default = {

    cluster = {

      region                   = "ru-central1"
      master_auto_upgrade      = false
      node_ipv4_cidr_mask_size = 25
      release_channel          = "REGULAR"
      network_policy_provider =  "CALICO"
    }

    node_groups = {

      instance_template_platform_id   = "standard-v3"
      maintenance_policy_auto_upgrade = false
      maintenance_policy_auto_repair  = true
      preemptible                     = false
      node_labels                     = {}
    }
  }
}


variable "k8s_config" {
  description = "Переменная для конфигурирования кластера и его node groups"

  type = map(object({

    ssh_keys_path = string

    # Ключ - не имя кластера
    cluster = object({
      description = string

      cluster_name                  = string
      cluster_ipv4_range            = string
      service_ipv4_range            = string
      cloud_name                    = string
      folder_name                   = string
      network_name                  = string
      k8s_master_version            = string
      maintenance_window_day        = string
      maintenance_window_start_time = string
      maintenance_window_duration   = string
      subnet_name                   = list(string)
      access_22_cidrs               = list(string)
      access_API_cidrs              = list(string)

      access_ingress_whitelist = map(object({
        description    = string

        v4_cidr_blocks = list(string)
        from_port      = number
        to_port        = number
        protocol       = string
      }))

      labels = map(string)

      region                   = optional(string)
      master_auto_upgrade      = optional(bool)
      node_ipv4_cidr_mask_size = optional(number)
      release_channel          = optional(string)
      cilium                   = list(bool)
    })

    # Ключ - не имя node group
    node_groups = map(object({
      description = string

      node_group_name               = string
      subnet_name                   = list(string)
      k8s_node_version              = string
      resources_memory              = number
      resources_cpu_cores           = number
      boot_disk_type                = string
      boot_disk_size                = number
      auto_scale_initial_count      = number
      auto_scale_min_count          = number
      auto_scale_max_count          = number
      maintenance_window_day        = string
      maintenance_window_start_time = string
      maintenance_window_duration   = string

      labels = map(string)
      role   = string

      instance_template_platform_id   = optional(string)
      maintenance_policy_auto_upgrade = optional(bool)
      maintenance_policy_auto_repair  = optional(bool)
      preemptible                     = optional(bool)
      node_labels                     = optional(map(string))
    }))
  }))

  default = {

    cluster-1 = {  

      cluster = {
        description = "Кластер dev-среды"
        
        cluster_name                  = "dev"
        cloud_name                    = "mvp"
        folder_name                   = "yc-dev"
        network_name                  = "k8s-network"
        k8s_master_version            = "1.19"
        maintenance_window_day        = "sunday"
        maintenance_window_start_time = "02:00"
        maintenance_window_duration   = "3h"
        cluster_ipv4_range            = "10.120.0.0/19"
        service_ipv4_range            = "10.120.64.0/19"
        
        subnet_name                   = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]
        
        access_22_cidrs               = [
          "10.10.10.0/24"
        ]

        access_API_cidrs              = [
          "10.10.10.0/24"
        ]

        labels = {}
      }

      node_groups = {

        node-group-1 = {
          description = "Группа k8s-nodes #1 dev среды"
          
          node_group_name = "dev-ng1"
          # В каждом сабнете из списка будет создана node group
          subnet_name = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]

          k8s_node_version              = "1.19"
          resources_memory              = 12
          resources_cpu_cores           = 4
          boot_disk_type                = "network-hdd"
          boot_disk_size                = 64
          auto_scale_initial_count      = 1
          auto_scale_min_count          = 1
          auto_scale_max_count          = 3
          maintenance_window_day        = "sunday"
          maintenance_window_start_time = "02:00"
          maintenance_window_duration   = "3h"
          node_labels                   = {

            "node.kubernetes.io/worker" = "true"
          }
          
          labels = {}
        }

        node-group-2 = {
          description = "Группа k8s-nodes #2 dev среды"
          
          node_group_name = "dev-ng2"
          # В каждом сабнете из списка будет создана node group
          subnet_name = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]
          
          k8s_node_version              = "1.19"
          resources_memory              = 12
          resources_cpu_cores           = 4
          boot_disk_type                = "network-hdd"
          boot_disk_size                = 64
          auto_scale_initial_count      = 1
          auto_scale_min_count          = 1
          auto_scale_max_count          = 3
          maintenance_window_day        = "sunday"
          maintenance_window_start_time = "02:00"
          maintenance_window_duration   = "3h"
          node_labels                   = {

            "node.kubernetes.io/master" = "true"
          }
          
          labels = {}
        }
      }
    }
    
    cluster-2 = {  

      cluster = {
        description = "Кластер test-среды"
        
        cluster_name                  = "test"
        cloud_name                    = "mvp"
        folder_name                   = "yc-dev"
        network_name                  = "k8s-network"
        k8s_master_version            = "1.19"
        maintenance_window_day        = "sunday"
        maintenance_window_start_time = "02:00"
        maintenance_window_duration   = "3h"
        cluster_ipv4_range            = "10.121.0.0/19"
        service_ipv4_range            = "10.121.64.0/19"
        
        subnet_name                   = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]
        
        access_22_cidrs               = [
          "10.10.10.0/24"
        ]

        access_API_cidrs              = [
          "10.10.10.0/24"
        ]

        labels = {}
      }

      node_groups = {
        node-group-1 = {
          description = "Группа k8s-nodes #1 test среды"
          
          node_group_name = "test-ng1"
          # В каждом сабнете из списка будет создана node group
          subnet_name = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]
          
          k8s_node_version              = "1.19"
          resources_memory              = 12
          resources_cpu_cores           = 4
          boot_disk_type                = "network-hdd"
          boot_disk_size                = 64
          auto_scale_initial_count      = 1
          auto_scale_min_count          = 1
          auto_scale_max_count          = 3
          maintenance_window_day        = "sunday"
          maintenance_window_start_time = "02:00"
          maintenance_window_duration   = "3h"

          labels = {}
        }

        node-group-2 = {
          description = "Группа k8s-nodes #2 test среды"
          
          node_group_name = "test-ng23"
          # В каждом сабнете из списка будет создана node group
          subnet_name = [
            "k8s-subnet-1", 
            "k8s-subnet-2", 
            "k8s-subnet-3"
          ]
          
          k8s_node_version                = "1.19"
          resources_memory                = 12
          resources_cpu_cores             = 4
          boot_disk_type                  = "network-hdd"
          boot_disk_size                  = 64
          auto_scale_initial_count        = 1
          auto_scale_min_count            = 1
          auto_scale_max_count            = 3
          maintenance_window_day          = "sunday"
          maintenance_window_start_time   = "02:00"
          maintenance_window_duration     = "3h"
          maintenance_policy_auto_upgrade = true

          labels = {}
        }
      }
    }
  }
}


##
## objectstorage module variables
##

variable "buckets_service_accounts" {

  type = map(object({
    
    name        = string
    description = string

    roles = list(string)
  }))

  default = {

    bucket-sa-1 = {

      name        = "bucket-owner",
      description = "gitlab bucket owner"
      roles       = [
        "storage.admin"
      ]
    },

    bucket-sa-2 = {

      name        = "server",
      description = "gitlab server"
      roles       = []
    },

    bucket-sa-3 = {

      name        = "runner",
      description = "gitlab-runner"
      roles       = []
    }
  }
}


variable "buckets" {

  type = object({

    cloud_name  = string
    folder_name = string
  
    default_bucket_name_prefix = optional(string)

    config = map(object({

      name  = string
      owner = string

      grants = map(object({

        permissions = list(string)
      }))

      lifecycle_rules = optional(list(object({

        id      = string
        enabled = bool

        abort_incomplete_multipart_upload_days = optional(number)

        transition = optional(object({

          days          = number
          storage_class = string
        }))

        expiration = optional(object({

          days = number
        }))
      })))
    }))
  })

  default = {

    cloud_name  = "mvp"
    folder_name = "yc-dev"
    
    default_bucket_name_prefix = "blah-blah"

    config = {
    
      bucket-1 = {

        name   = "backups"
        owner  = "bucket-owner"
        grants = {}

        lifecycle_rules = [
          {
            id      = "transit-old-backups-to-cold"
            enabled = true

            abort_incomplete_multipart_upload_days = 2

            transition = {
              days          = 30
              storage_class = "COLD"
            }
          }, {
            id      = "remove-old-backups"
            enabled = true

            expiration = {
              days = 90
            }
          }
        ]
      },

      bucket-2 = {

        name   = "git-lfs"
        owner  = "bucket-owner"
        grants = {}
      },

      bucket-3 = {

        name   = "runner-cache"
        owner  = "bucket-owner"
        grants = {
          "runner" = {

            permissions = [
              "FULL_CONTROL"
            ]
          }
        }

        lifecycle_rules = [
          {
            id      = "transit-old-runner-caches-to-cold"
            enabled = true

            abort_incomplete_multipart_upload_days = 2

            transition = {
              days          = 7
              storage_class = "COLD"
            }
          }, {
            id      = "remove-old-runner-caches"
            enabled = true

            expiration = {
              days = 45
            }
          }
        ]
      }
    }
  }
}


##
## psql module variables
##

variable "psql_config" {
  description = "Переменная, описывающая конфигурацию кластеров постгреса"

  type = object({
    description = string

    cluster_name = string
    cloud_name   = string
    folder_name  = string
    network_name = string
    subnet_name  = list(string)
    access_6432_subnets = list(string)

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number
    
    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = object({

      type = string
      day  = string
      hour = number
    })
    
    users = map(object({

      username             = string
      password             = string
      accessible_databases = list(string)
      grants               = optional(list(string))
    }))

    pool_discard = optional(bool)
    pooling_mode = optional(string)

    environment = optional(string)
    version     = optional(number)
    

    max_connections                   = optional(number)
    enable_parallel_hash              = optional(bool)
    vacuum_cleanup_index_scale_factor = optional(number)
    autovacuum_vacuum_scale_factor    = optional(number)
    default_transaction_isolation     = optional(string)
    shared_preload_libraries          = optional(string)

    labels = optional(map(string))
  })

  default = {
    description = "Кластер psql"

    cluster_name = "psql-name"
    cloud_name   = "mvp"
    folder_name  = "yc-dev"
    network_name = "db-network"
    
    subnet_name  = [
      "db-subnet-1",
      "db-subnet-2",
      "db-subnet-3"
    ]

    access_6432_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3"
    ]

    resource_preset_id = "s2.small"
    disk_type_id       = "network-ssd"
    disk_size          = 513

    backup_window_start = {
      hours   = 23
      minutes = 00
    }

    maintenance_window = {
      type = "WEEKLY"
      day  = "SAT"
      hour = 01 # "00" - нельзя!!!
    }

    users = {
      user-1 = {

        username = "ng"
        password = "12345678"
        
        accessible_databases = [
          "atma_api", "keycloak"
        ]

        grants = []
      }

      user-2 = {

        username = "keycloak"
        password = "12345678"
        
        accessible_databases = [
          "keycloak"
        ]
      }

      user-3 = {
        
        username = "admin_ru"
        password = "12345678"
        
        accessible_databases = [
          "keycloak"
          ]

        grants = [
          "mdb_admin"
        ]
      }
    }

    labels = {}
  }
}


variable "databases" {
  description = "Конфигурация баз данных кластера psql"
  
  type = map(object({

    database_name = string
    owner         = string
    extension     = list(string)
  }))

  default = {

    db-1 = {

      database_name = "admindb"
      owner         = "admin_ru"
      
      extension = []
    }
  
    db-2 = {
      
      database_name = "atma_api"
      owner         = "ng"
      
      extension = [
        "uuid-ossp"
      ]
    }
  
    db-3 = {

      database_name = "keycloak"
      owner         = "keycloak"
      
      extension = []
    }
  }
}


##
## mongo module variables
##

variable "mongo_config" {
  description = "Переменная, описывающая конфигурацию кластера MongoDB"

  type = object({
    
    description = string

    cluster_name        = string
    cloud_name          = string
    folder_name         = string
    network_name        = string
    subnet_name         = list(string)
    access_27018_subnets = list(string)

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = object({

      type = string
      day  = string
      hour = number
    })
    
    databases = list(string)

    users = map(object({

      username             = string
      password             = string
      accessible_databases = set(object({
        database_name = string
        
        # https://cloud.yandex.com/en-ru/docs/managed-mongodb/concepts/users-and-roles
        roles         = list(string)
      }))
    })) 

    environment = optional(string)
    version     = optional(number)

    labels      = optional(map(string))
  })

  default = {
    description = "Кластер psql"

    cluster_name = "gitlab-mongodb"
    cloud_name   = "mvp"
    folder_name  = "yc-dev"
    network_name = "gitlab-network"
    
    subnet_name  = [
      "db-subnet-1",
      "db-subnet-2",
      "db-subnet-3"
    ]

    access_27018_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3"
    ]

    resource_preset_id = "s2.small"
    disk_type_id       = "network-ssd"
    disk_size          = 64

    backup_window_start = {
      hours   = 23
      minutes = 00
    }

    maintenance_window = {
      type = "WEEKLY"
      day  = "SAT"
      hour = 01 # "00" - нельзя!!!
    }

    databases = [
      "db-1",
      "db-2",
      "db-3"
    ]

    users = {
      user-1 = {

        username = "bob"
        password = "12345678"
        
        accessible_databases = [
          {
            database_name = "db-1"
            roles = ["readWrite"]
          },
          {
            database_name = "db-3"
            roles = ["mdbDbAdmin"]
          }
        ]
      }

      user-2 = {

        username = "alex"
        password = "12345678"
        
        accessible_databases = [
          {
            database_name = "db-2"
            roles = ["read", "readWrite"]
          }
        ]
      }
    }

    labels = {}
  }
}
